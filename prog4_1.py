validtoks=["push","pop","add","sub","mul",
           "div","mod","skip","save","get"]


def Tokenize(str):
    sp=str.split()
    #get list of invalid tokens
    invalid=[x for x in sp if x not in
            validtoks and not isNum(x)]

    #if it has any invalid toks, then raise Error
    if len(invalid)!=0:
        raise ValueError("Unexpected Token: "+invalid[0])

    valid=[x for x in sp]
    return valid

def isNum(s):
    try:
        int(s)
        return True
    except ValueError:
        return False

onepart=["pop","add","sub","mul","div","mod","skip"]
twopart=["push","save","get"]
def Parse(toks):
    first=toks[0]

    if first in onepart:
        if len(toks)!=1:
            raise ValueError("Parse error")
    elif first in twopart:
        if len(toks)!=2:
            raise ValueError("Parse error")
        if not isNum(toks[1]):
            raise ValueError("Parse error")
    else:
        raise ValueError("Parse error")

