from prog4_1 import *
class StackMachine:
    def __init__(self):
        self.CurrentLine=0
        self.stack=[]
        self.ram=[None]*1000


    def Execute(self,tokens):

        function=tokens[0]
        execute = getattr(self, function)
        #one token only does not require call with int

        if len(tokens)==1:
            willret= execute()
        else:
            willret= execute(int(tokens[1]))
        self.CurrentLine += 1
        return willret

    def push(self,int):
        self.stack.append(int)
        return None
    def pop(self):
        num1 = self.stack.pop()
        return num1

    def add(self):

        num1 = self.stack.pop()
        num2 = self.stack.pop()

        self.stack.append(num1+num2)
        return None
    def sub(self):

        num1 = self.stack.pop()
        num2 = self.stack.pop()

        self.stack.append(num1 - num2)
        return None
    def mul(self):

        num1 = self.stack.pop()
        num2 = self.stack.pop()

        self.stack.append(num1 * num2)
        return None
    def div(self):

        num1 = self.stack.pop()
        num2 = self.stack.pop()

        self.stack.append(num1//num2)
        return None
    def mod(self):

        num1 = self.stack.pop()
        num2 = self.stack.pop()

        self.stack.append(num1%num2)
        return None
    def skip(self):

        num1 = self.stack.pop()
        num2 = self.stack.pop()
        if num1==0:
            self.CurrentLine+=num2
        return None
    def save(self,num):
        self.ram[num]=self.stack.pop()
        return None
    def get(self,num):
        saved=self.ram[num]
        self.stack.append(saved)
        return None







