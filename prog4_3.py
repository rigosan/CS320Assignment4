from prog4_1 import *
from prog4_2 import *
import sys
def driver():
    print("Assignment #4-3, Sergio Santana, rigosanle@gmail.com")
    # command line sys.argv[1]
    namefile=sys.argv[1]



    f=open(namefile,mode="r")
    lines=f.readlines()
    tokens=[]
    for line in lines:
        tokens.append(Tokenize(line))

    tokens = [x for x in tokens if x != []]

    for token in tokens:
        Parse(token)

    stackMach=StackMachine()
    numLines=len(tokens)


    while True:
        if stackMach.CurrentLine >= numLines:
            break
        if stackMach.CurrentLine<0:
            print("trying to execute invalid line:",stackMach.CurrentLine)
            exit(0)
        try:
            mightprint=stackMach.Execute(tokens[stackMach.CurrentLine])
            if mightprint is None:
                pass
            else:
                print(mightprint)

        except IndexError:
            string="'"+"".join(tokens[stackMach.CurrentLine])+"'"
            output="Line "+str(stackMach.CurrentLine)+":"
            print(output,string,"caused Invalid Memory Access")
            exit(0)

        # except Exception:
        #     print(" Negaative Line",stackMach.CurrentLine)
    print("Program terminated correctly")






if __name__ == "__main__":
    driver()